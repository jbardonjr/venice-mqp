// main.js for PermaDeath in Venice

// The following comment lines are for JSLint. Don't remove them!

/*jslint nomen: true, white: true */
/*global PS */

// The G variable encapsulates all game-specific code

var G = ( function () {
	"use strict";

	// Private constants

	// Default character colors

	var COLOR_PLAYER = PS.COLOR_GREEN;
	var COLOR_TARGET = PS.COLOR_YELLOW;
	var COLOR_NPC1 = PS.COLOR_CYAN;
	var COLOR_NPC2 = PS.COLOR_MAGENTA;

	// Object planes

	var PLANE_BG = 0;
	var PLANE_BOATS = 1;
	var PLANE_BRIDGES = 2;
	var PLANE_NPC = 3;
	var PLANE_PLAYER = 4;
	var PLANE_TARGET = 5;
	var PLANE_ROOF = 6;

	var THRESHOLD = 16; // Black threshold for pathmap image

	var WALL_VALUE = 0;
	var FLOOR_VALUE = 1;
	var SOFT_WALL_VALUE = 8096;

	// Private variables

	// Episode list

	var episodes = {

		// All episodes should provide these properties

		ep1 : {
			grid_x : 25, // grid width
			grid_y : 25, // grid height
			start_top : 62, // TLC of background in grid
			start_left : 0,
			start_center : false,
//			file_bg : "maps/ep1_bg.bmp",
//			file_pathmap : "maps/ep1_pathmap.bmp",
//			file_chars : "maps/ep1_chars.bmp",
//			file_bg : "maps/ep_1_vmMain.bmp",
//			file_pathmap : "maps/ep_1_pfMain.bmp",
//			file_chars : "maps/ep_1_chMain.bmp",
			file_bg : "maps/test_bg.bmp",
			file_pathmap : "maps/test_pathmap.bmp",
			file_chars : "maps/test_chars.bmp",
			file_camera : "maps/test_camera.bmp",
			timer: ep1_tick,
			mapTimer : scroller,
			margin_left : 5, // left map scroll margin
			margin_right: 5, // right map scroll margin
			margin_top : 5, // top map scroll margin
			margin_bottom : 5, // bottom map scroll margin
			scroll_left : 12, // maximum left scroll distance
			scroll_right: 12, // maximum right scroll distance
			scroll_up: 12, // maximum up scroll distance
			scroll_down: 12 // maximum down scroll	 distance
		}
	};

	var episode = null; // current episode

	// Current episode imageData

	var image_bg = null; // background image
	var image_pathmap = null; // pathmap image
	var image_chars = null; // character position image
	var image_camera = null; // camera track image

	var pathmap = ""; // episode pathmap
	var camera = ""; // episode camera track

	var player = null; // player character
	var target = null; // target character
	var npc1 = []; // list of npc1 characters
	var npc2 = []; // list of npc2 characters

	var left = 0; // left corner of visible map
	var top = 0; // top corner of visible map
	var width = 0; // width of map
	var height = 0; // height of map
	var xmax = 0; // maximum x coordinate
	var ymax = 0; // maxmimum y coordinate

	var scrolling = false; // true if scrolling
	var scroll_x = 0; // x-scroll target
	var scroll_y = 0; // y-scroll target
	var scroll_path = null; // scroll path
	var scroll_step = 0;

	var timer = null; // id of main timer; null if none running
	var mapTimer = null;

	// Private functions

	// Improved typeof by Doug Crockford, with NaN detection by Moriarty
	// val = any Javascript value

	function typeOf ( val ) {
		var type;

		type = typeof val;
		if ( type === "number" ) {
			if ( isNaN( val ) ) {
				type = "NaN";
			}
		}
		else if ( type === "object" ) {
			if ( val ) {
				if ( val instanceof Array ) {
					type = "array";
				}
			}
			else {
				type = "null";
			}
		}
		return type;
	}

	function drawBackground () {
		PS.gridPlane( PLANE_BG ); // set image plane
		PS.imageBlit( image_bg, 0 - left, 0 - top );
	}

	// Draw character at current map position
	// Adjust for map scrolling

	function drawCharacter ( c ) {
		var nx, ny;

		// Calculate actual grid position

		nx = c.mx - left;
		ny = c.my - top;

		// If it's changed, move it

		if ( ( nx !== c.gx ) || ( ny !== c.gy ) ) {

			c.gx = nx;
			c.gy = ny;

			// Move & show sprite if over grid

			if ( ( nx >= 0 ) && ( nx < episode.grid_x ) && ( ny >= 0 ) && ( ny < episode.grid_y ) ) {
				PS.spriteMove( c.sprite, nx, ny );
				if ( !c.visible ) {
					c.visible = true;
					PS.spriteShow( c.sprite, true );
				}
			}

			// Otherwise hide sprite

			else if ( c.visible ) {
				c.visible = false;
				PS.spriteShow( c.sprite, false );
			}
		}

		c.dirty = false;
	}

	// Move character to map position x/y

	function moveCharacter ( c, x, y ) {
		c.mx = x;
		c.my = y;
	}

	// Start character along a route (if it has any steps)

	function startRoute ( c, route ) {
		if ( route.length > 0 ) {
			c.route = route;
		}
		else {
			c.route = null;
		}
		c.step = 0;
	}

	function scroll ( x, y ) {
		var len, i, c;

		if ( x < 0 ) {
			x = 0;
		}
		if ( x > xmax ) {
			x = xmax;
		}
		left = x;

		if ( y < 0 ) {
			y = 0;
		}
		if ( y > ymax ) {
			y = ymax;
		}
		top = y;

		drawBackground();

		drawCharacter( player );
		drawCharacter( target );

		len = npc1.length;
		for ( i = 0; i < len; i += 1 ) {
			c = npc1[ i ];
			drawCharacter( c );
		}

		len = npc2.length;
		for ( i = 0; i < len; i += 1 ) {
			c = npc2[ i ];
			drawCharacter( c );
		}
	}

	// Called when map needs scrolling

	function scroller () {
//		var dx, dy;

//		if ( scrolling ) {
//			if ( left > scroll_x ) {
//				dx = -1;
//			}
//			else if ( left < scroll_x ) {
//				dx = 1;
//			}
//			else {
//				dx = 0;
//			}
//
//			if ( top > scroll_y ) {
//				dy = -1;
//			}
//			else if ( top < scroll_y ) {
//				dy = 1;
//			}
//			else {
//				dy = 0;
//			}
//
//			if ( dx || dy ) {
//				scroll( left + dx, top + dy );
//			}
//			else {
//				scrolling = false;
//			}
//		}

		var route, len, step, pos, x, y;

		route = scroll_path;
		if ( route ) {
			len = route.length;
			step = scroll_step;
			if ( step < len ) {
				pos = route[ step ]; // get next step on route
				x = pos[ 0 ];
				y = pos[ 1 ];
				scroll( x, y );
				step += 1;
				if ( step >= len ) { // path done?
					scroll_path = null; // nuke it
					step = 0;
				}
				scroll_step = step;
			}
		}
	}

	// Check if scrolling required for object at map coordinates [x, y]
	// Returns true if scroll needed, else false

	function scrollCheck ( mx, my ) {
		var x, y, nx, ny, route;

		// calc effective grid coordinates

		x = mx - left;
		y = my - top;

		if ( x < episode.margin_left ) // scroll left?
		{
			if ( left > 0 ) // room to scroll left?
			{
				nx = left - episode.scroll_left;
				if ( nx < episode.margin_left )	{
					nx = 0;
				}
				scroll_x = nx;
			}
		}
		else if ( x > episode.margin_right ) // scroll right?
		{
			if ( left < xmax ) { // room to scroll right?
				nx = left + episode.scroll_right;
				if ( nx > xmax ) {
					nx = xmax;
				}
				scroll_x = nx;
			}
		}
		else {
			scroll_x = left;
		}


		if ( y < episode.margin_top ) // scroll up?
		{
			if ( top > 0 ) // room to scroll up?
			{
				ny = top - episode.scroll_up;
				if ( ny < episode.margin_top ) {
					ny = 0;
				}
				scroll_y = ny;
			}
		}
		else if ( y > episode.margin_bottom ) // scroll down?
		{
			if ( top < ymax ) // room to scroll down?
			{
				ny = top + episode.scroll_down;
				if ( ny > ymax ) {
					ny = ymax;
				}
				scroll_y = ny;
			}
		}
		else {
			scroll_y = top;
		}

		if ( ( left !== scroll_x ) || ( top !== scroll_y ) ) {
			route = PS.line( left, top, scroll_x, scroll_y );
			if ( route.length > 0 ) {
				scroll_path = route;
				scroll_step = 0;
			}
			return true;
		}

		return false;
	}

	// Timer function for Episode 1

	function ep1_tick () {
		var route, len, step, pos, x, y;

		route = player.route;
		if ( route ) {
			len = route.length;
			step = player.step;

			if ( step < len ) {
				pos = route[ step ]; // get next step on route
				x = pos[ 0 ];
				y = pos[ 1 ];

				// Stop if move would enter a wall

//				if ( !isWall ( player.pathmap, x, y ) ) {
//
//					moveCharacter( player, x, y );// point to next path step
//
//					// Redraw if scrolling not needed
//
//					if ( !scrollCheck( x, y ) )	{
//						drawCharacter( player );
//					}
//					else {
//						scroller();
//					}
//
//					step += 1;
//					if ( step >= len ) { // path done?
//						player.route = null; // nuke it
//						step = 0;
//					}
//				}
//				else { // stop if wall
//					player.route = null; // nuke route
//					step = 0;
//				}

				moveCharacter( player, x, y );// point to next path step

				// Redraw if scrolling not needed

				if ( !scrollCheck( x, y ) ) {
					drawCharacter( player );
				}
//				else {
//					scroller();
//				}

				step += 1;
				if ( step >= len ) { // path done?
					player.route = null; // nuke it
					step = 0;
				}
				player.step = step;
			}
		}
		scroller();
	}

	// Start a loaded episode

	function startEpisode () {
		var len, i, c;

		PS.gridSize( episode.grid_x, episode.grid_y ); // set grid size
		PS.border( PS.ALL, PS.ALL, 0 ); // hide bead borders

		// Establish map parameters

		width = image_bg.width;
		height = image_bg.height;

		xmax = width - episode.grid_x;
		if ( xmax < 0 ) {
			xmax = 0;
		}

		ymax = height - episode.grid_y;
		if ( ymax < 0 ) {
			ymax = 0;
		}

		left = episode.start_left;
		top = episode.start_top;

		episode.margin_right = episode.grid_x - episode.margin_right;
		episode.margin_bottom = episode.grid_y - episode.margin_bottom;

		drawBackground();

		// Create & place player sprite

		player.sprite = PS.spriteSolid( 1, 1 );
		PS.spriteSolidColor( player.sprite, player.color );
		PS.spritePlane( player.sprite, PLANE_PLAYER );
		moveCharacter( player, player.mx, player.my );
		drawCharacter( player );

		// Create target sprite

		target.sprite = PS.spriteSolid( 1, 1 );
		PS.spriteSolidColor( target.sprite, target.color );
		PS.spritePlane( target.sprite, PLANE_TARGET );
		moveCharacter( target, target.mx, target.my );
		drawCharacter( target );

		// Create NPC1 sprites

		len = npc1.length;
		for ( i = 0; i < len; i += 1 ) {
			c = npc1[ i ];
			c.sprite = PS.spriteSolid( 1, 1 );
			PS.spriteSolidColor( c.sprite, c.color );
			PS.spritePlane( c.sprite, PLANE_NPC );
			moveCharacter( c, c.mx, c.my );
			drawCharacter( c );
		}

		// Create NPC2 sprites

		len = npc2.length;
		for ( i = 0; i < len; i += 1 ) {
			c = npc2[ i ];
			c.sprite = PS.spriteSolid( 1, 1 );
			PS.spriteSolidColor( c.sprite, c.color );
			PS.spritePlane( c.sprite, PLANE_NPC );
			moveCharacter( c, c.mx, c.my );
			drawCharacter( c );
		}

		// Stop previous episode's timer (if any )

		if ( timer ) {
			PS.timerStop( timer );
			timer = null;
		}
		if ( mapTimer ) {
			PS.timerStop( mapTimer );
			mapTimer = null;
		}

		// Start new episode's timer (if any)

		if ( episode.timer ) {
			timer = PS.timerStart( 4, episode.timer ); // tick at 15 fps
		}
//		if ( episode.mapTimer ) {
//			mapTimer = PS.timerStart( 4, episode.mapTimer ); // tick at 15 fps
//		}
	}

	// Set up a new character object
	// (required) str : string = unique id string
	// (required) x, y : number = initial x/y position
	// (optional) rgb : number = RGB multiplex color (default = red)
	// (optional) pm : string = pathmap (default = episode pathmap)
	// Returns initialized character object

	function initCharacter ( str, x, y, rgb, pm ) {
		var c;

		if ( rgb === undefined ) {
			rgb = PS.COLOR_RED;
		}

		if ( pm === undefined ) {
			pm = pathmap; // use episode map
		}

		c = {
			id : str, // unique id string
			mx : x, // x position on map
			my : y, // y position on map
			gx : -5000, // x position on grid
			gy : -5000, // y position on grid
			color : rgb, // color,
			visible : false, // true if visible
			sprite : "", // sprite id
			pathmap : pm, // pathmap
			route : null, // current route, null if none
			step : 0, // current path step
			dirty : true // true if redraw needed
		};

		PS.debug( str + " @ " + x + ", " + y + "\n" );

		return c;
	}

	// Load camera track image

	function loadCamera ( imageData ) {
		var map, ptr, len, i, r, g, b, data, result;

		if ( imageData === PS.ERROR ) {
			return imageData;
		}

		// Report imageData in debugger

		PS.debug( "Loaded camera track " + imageData.source +
			":\nid = " + imageData.id +
			"\nwidth = " + imageData.width +
			"\nheight = " + imageData.height +
			"\nformat = " + imageData.pixelSize + "\n" );

		image_camera = imageData; // save camera image in case needed later

		// Create a new imageMap by analyzing data in pathmap image

		map = {
			width : imageData.width,
			height : imageData.height,
			pixelSize : 1,
			data : []
		};

		ptr = 0;
		len = imageData.width * imageData.height;
		for ( i = 0; i < len; i += 1 ) {
			// Get r/g/b values of image pixel

			r = imageData.data[ ptr ];
			ptr += 1;
			g = imageData.data[ ptr ];
			ptr += 1;
			b = imageData.data[ ptr ];
			ptr += 1;

			// If any value is above black threshold, assume floor

			if ( ( r >= THRESHOLD ) || ( g >= THRESHOLD ) && ( b >= THRESHOLD ) ) {
				data = FLOOR_VALUE;
			}
			else {
				data = WALL_VALUE; // else wall
			}
			map.data[ i ] = data;
		}

		startEpisode(); // everything's loaded!
		return PS.DONE;
	}

	// Load character image

	function loadCharacters ( imageData ) {
		var ptr, len, x, y, r, g, b, id, result;

		if ( imageData === PS.ERROR ) {
			return imageData;
		}

		// Report imageData in debugger

		PS.debug( "Loaded character map " + imageData.source +
			":\nid = " + imageData.id +
			"\nwidth = " + imageData.width +
			"\nheight = " + imageData.height +
			"\nformat = " + imageData.pixelSize + "\n" );

		image_chars = imageData; // save character image in case needed later

		// Scan image data to extract character locations
		// Player = 0x00FF00 (pure green)
		// Target = 0xFFFF00 (pure yellow)
		// NPC1 = 0xFF00nn (pure red plus 1+ index in blue)
		// NPC2 = 0xnn00FF (pure blue plus 1+ index in red)

		ptr = 0;
		for ( y = 0; y < imageData.height; y += 1 ) {
			for ( x = 0; x < imageData.width; x += 1 ) {

				// Get r/g/b values of image pixel

				r = imageData.data[ ptr ];
				ptr += 1;
				g = imageData.data[ ptr ];
				ptr += 1;
				b = imageData.data[ ptr ];
				ptr += 1;

				// If green = 255, it's either the player or the target

				if ( g === 255 ) {
					if ( r === 255 ) {
						id = "target";
						target = initCharacter( id, x, y, COLOR_TARGET );
					}
					else {
						id = "player";
						player = initCharacter( id, x, y, COLOR_PLAYER );
					}
				}
				else if ( r === 255 ) { // NPC1
					id = "npc1_" + b;
					npc1.push( initCharacter( id, x, y, COLOR_NPC1 ) );
				}
				else if ( b === 255 ) { // NPC2
					id = "npc2_" + r;
					npc2.push( initCharacter( id, x, y, COLOR_NPC2 ) );
				}
			}
		}

		// Load camera track

		result = PS.imageLoad( episode.file_camera, loadCamera, 3 );
		if ( result === PS.ERROR ) {
			return result;
		}

		return PS.DONE;
	}

	// Load pathmap image

	function loadPathmap ( imageData ) {
		var map, ptr, len, i, r, g, b, data, result;

		if ( imageData === PS.ERROR ) {
			return imageData;
		}

		// Report imageData in debugger

		PS.debug( "Loaded pathmap " + imageData.source +
			":\nid = " + imageData.id +
			"\nwidth = " + imageData.width +
			"\nheight = " + imageData.height +
			"\nformat = " + imageData.pixelSize + "\n" );

		image_pathmap = imageData; // save pathmap image in case needed later

		// Create a new imageMap by analyzing data in pathmap image

		map = {
			width : imageData.width,
			height : imageData.height,
			pixelSize : 1,
			data : []
		};

		ptr = 0;
		len = imageData.width * imageData.height;
		for ( i = 0; i < len; i += 1 ) {
			// Get r/g/b values of image pixel

			r = imageData.data[ ptr ];
			ptr += 1;
			g = imageData.data[ ptr ];
			ptr += 1;
			b = imageData.data[ ptr ];
			ptr += 1;

			// If any value is above black threshold, assume floor

			if ( ( r >= THRESHOLD ) || ( g >= THRESHOLD ) && ( b >= THRESHOLD ) ) {
				data = FLOOR_VALUE;
			}
			else {
				data = WALL_VALUE; // else wall
			}
			map.data[ i ] = data;
		}

		pathmap = PS.pathMap( map ); // Create pathmap

		// Load characters

		result = PS.imageLoad( episode.file_chars, loadCharacters, 3 );
		if ( result === PS.ERROR ) {
			return result;
		}

		return PS.DONE;
	}

	// Load background image

	function loadBackground ( imageData ) {
		var x, y, ptr, color, result;

		// Report imageData in debugger

		if ( imageData === PS.ERROR ) {
			return imageData;
		}

		PS.debug( "Loaded background " + imageData.source +
			":\nid = " + imageData.id +
			"\nwidth = " + imageData.width +
			"\nheight = " + imageData.height +
			"\nformat = " + imageData.pixelSize + "\n" );

		image_bg = imageData; // save image data

		// Load pathmap & characters

		result = PS.imageLoad( episode.file_pathmap, loadPathmap, 3 );
		if ( result === PS.ERROR ) {
			return result;
		}

		return PS.DONE;
	}

	// Returns true if pathmap location x/y is a wall

	function isWall ( pm, x, y ) {
		var result, val;

		result = PS.pathData( pm, x, y, 1, 1 );
		val = result[ 0 ];
//		if ( val <= WALL_VALUE ) {
//			return true;
//		}
//		return ( val >= SOFT_WALL_VALUE );
		return ( val <= WALL_VALUE );
	}

	// Change pathmap location x/y to a wall

	function makeWall ( pm, x, y ) {
		var result;

		PS.pathData( pm, x, y, 1, 1, WALL_VALUE );
	}

	// Change pathmap location x/y to a floor

	function makeFloor ( pm, x, y ) {
		var result;

		PS.pathData( pm, x, y, 1, 1, FLOOR_VALUE );
	}

	// Properties of sys become public properties of G

	var sys = {

		// Load an episode
		// str : string = id of an episode in the episodes object

		loadEpisode : function ( str ) {
			var result;

			// Validate id string

			if ( !str || ( typeOf( str ) !== "string" ) || ( str.length < 1 ) ) {
				PS.debug( "[G.loadEpisode] Invalid parameter" );
				return PS.ERROR;
			}

			// Validate episode

			episode = episodes[ str ];
			if ( !episode || ( typeOf( episode ) !== "object" ) ) {
				PS.debug( "[G.loadEpisode] '" + str + "' is not a valid episode id" );
				return PS.ERROR;
			}

			// Load background image (which also loads walkmap, characters and camera)

			result = PS.imageLoad( episode.file_bg, loadBackground, 1 );
			if ( result === PS.ERROR ) {
				return result;
			}

			return PS.DONE;
		},

		// G.click( x, y, data, options )
		// Called when grid is clicked

		click : function ( x, y, data, options )
		{
			var map, nx, ny, route, len, i, step, x, y;

			// If clicking current player loc, exit (for now)

			if ( ( x === player.gx ) && ( y === player.gy ) ) {
				return;
			}

			map = player.pathmap;

			// Convert clicked grid location to map coordinates

			nx = x + left;
			ny = y + top;

			// Is clicked location a wall?
			// If not, call pathfinder

			if ( !isWall( map, nx, ny ) ) {
				route = PS.pathFind( map, player.mx, player.my, nx, ny );
			}

			// Otherwise follow a straight line to target
			// Player will stop at first wall

			else {
				route = PS.line( player.mx, player.my, nx, ny );
			}

			// trim line at first wall or soft wall

			len = route.length;
			for ( i = 0; i < len; i += 1 ) {
				step = route[ i ];
				x = step[ 0 ];
				y = step[ 1 ];
				if ( isWall( map, x, y ) ) {
					route.length = i; // abort line
					break;
				}
			}

			startRoute( player, route );
		}
	};

	return sys;
} () );

// All of the functions below MUST exist, or the engine will complain!

PS.init = function ( system, options ) {
	"use strict";

	G.loadEpisode( "ep1" );
};

PS.touch = function ( x, y, data, options ) {
	"use strict";

	G.click( x, y, data, options );
};

PS.release = function ( x, y, data, options ) {
	"use strict";
};

PS.enter = function ( x, y, data, options ) {
	"use strict";
};

PS.exit = function ( x, y, data, options ) {
	"use strict";
};

PS.exitGrid = function ( options ) {
	"use strict";
};

PS.keyDown = function ( key, shift, ctrl, options ) {
	"use strict";
};

PS.keyUp = function ( key, shift, ctrl, options ) {
	"use strict";
};

PS.swipe = function ( data, options ) {
	"use strict";
};

PS.input = function ( sensors, options ) {
	"use strict";
};



