// main.js for PermaDeath in Venice

// The following comment lines are for JSLint. Don't remove them!

/*jslint nomen: true, white: true */
/*global PS */

// The G variable encapsulates all game-specific code

//TODO: Generic list
//TODO: Make sure previous episode NPCs are unloaded when launching the next
	//THIS MAY ALSO AFFECT TRIGGERS!!!
	//DO THIS VIA CLEARING EXISTING ARRAYS & MOVING TADZIO TO OFFSCREEN AT THE START OF LOADEPISODE

var G = ( function () {
	"use strict";

	// Private constants

	// Default character colors

	var COLOR_PLAYER = PS.COLOR_GREEN;
	var COLOR_TARGET = PS.COLOR_YELLOW;
	var COLOR_NPC1 = PS.COLOR_RED;
	var COLOR_NPC2 = PS.COLOR_CYAN;

	// Object planes

	var PLANE_BG = 0;
	var PLANE_BOATS = 1;
	var PLANE_BRIDGES = 2;
	var PLANE_NPC = 3;
	var PLANE_PLAYER = 4;
	var PLANE_TARGET = 5;
	var PLANE_ROOF = 6;

	var THRESHOLD = 16; // Black threshold for pathmap image
	var WALL_VALUE = 0;
	var FLOOR_VALUE = 1;

	// Private variables

	// Episode list

	var episodes = {

		//TODO: define ep2 and onwards based on the below
		// All episodes should provide these properties

		ep1 : {
			id : "ep1", // map id
			grid_x : 27, // grid width
			grid_y : 27, // grid height
			start_top : 0, // TLC of background in grid
			start_left : 0,
			start_center : false,
			file_bg : "maps/ep_1_vmMain.bmp",  //Maybe use a png for the background, too?
			file_fg : "maps/ep_1_vtMain.png",
			file_pathmap : "maps/ep_1_pfMain.bmp",
			file_chars : "maps/ep_1_chMain.bmp",
			file_triggers : "maps/ep_1_trgMain.bmp",
			timer: ep1_tick,
			margin_left : 7, // left map scroll margin
			margin_right: 7, // right map scroll margin
			margin_top : 7, // top map scroll margin
			margin_bottom : 7, // bottom map scroll margin
			scroll_left : 13, // maximum left scroll distance
			scroll_right: 13, // maximum right scroll distance
			scroll_up: 13, // maximum up scroll distance
			scroll_down: 13 // maximum down scroll distance
		},

		dream1 : {
			id : "dream1", // map id
			grid_x : 25, // grid width
			grid_y : 25, // grid height
			start_top : 34, // TLC of background in grid
			start_left : 0,
			start_center : false,
			file_bg : "maps/drm_1_vmMain.bmp",  //Maybe use a png for the background, too?
			file_fg : "maps/drm_1_vtMain.png",
			file_pathmap : "maps/drm_1_pfMain.bmp",
			file_chars : "maps/drm_1_chMain.bmp",
			file_triggers : "maps/drm_1_trgMain.bmp",
			timer: ep1_tick,  //TODO: change this?
			margin_left : 4, // left map scroll margin
			margin_right: 4, // right map scroll margin
			margin_top : 4, // top map scroll margin
			margin_bottom : 4, // bottom map scroll margin
			scroll_left : 12, // maximum left scroll distance
			scroll_right: 12, // maximum right scroll distance
			scroll_up: 12, // maximum up scroll distance
			scroll_down: 12 // maximum down scroll distance
		}
	};

	var episode = null; // current episode

	// Current episode imageData

	var image_bg = null; // background image
	var image_fg = null; // foreground image
	var image_pathmap = null; // pathmap image
	var image_chars = null; // character position image
	var image_triggers = null; // trigger position image

	var pathmap = ""; // episode pathmap

	var player = null; // player character
	var target = null; // target character
	var npc1 = []; // list of npc1 characters
	var npc2 = []; // list of npc2 characters
	var triggerArray = []; // list of triggers

	var left = 0; // left corner of visible map
	var top = 0; // top corner of visible map
	var width = 0; // width of map
	var height = 0; // height of map
	var xmax = 0; // maximum x coordinate
	var ymax = 0; // maxmimum y coordinate

	var scrolling = false; // true if scrolling
	var scroll_x = 0; // x-scroll target
	var scroll_y = 0; // y-scroll target
	var scroll_path = null; // scroll path
	var scroll_step = 0;

	var timer = null; // id of main timer; null if none running
	var textTimer = null; // ids of text timers; empty if none are running TODO: change to array

	var previousX = -1;
	var previousY = -1;

	// Private functions

	// Improved typeof by Doug Crockford, with NaN detection by Moriarty
	// val = any Javascript value

	function typeOf ( val ) {
		var type;

		type = typeof val;
		if ( type === "number" ) {
			if ( isNaN( val ) ) {
				type = "NaN";
			}
		}
		else if ( type === "object" ) {
			if ( val ) {
				if ( val instanceof Array ) {
					type = "array";
				}
			}
			else {
				type = "null";
			}
		}
		return type;
	}

	function drawBackground () {
		PS.gridPlane( PLANE_BG ); // set image plane
		PS.imageBlit( image_bg, 0 - left, 0 - top );
	}

	function drawForeground () {
		PS.gridPlane( PLANE_ROOF ); // set image plane
		PS.imageBlit( image_fg, 0 - left, 0 - top );
	}

	// Draw character at current map position
	// Adjust for map scrolling

	function drawCharacter ( c ) {
		var nx, ny;

		// Calculate actual grid position

		nx = c.mx - left;
		ny = c.my - top;

		// If it's changed, move it

		if ( ( nx !== c.gx ) || ( ny !== c.gy ) ) {

			c.gx = nx;
			c.gy = ny;

			// Move & show sprite if over grid

			if ( ( nx >= 0 ) && ( nx < episode.grid_x ) && ( ny >= 0 ) && ( ny < episode.grid_y ) ) {
				PS.spriteMove( c.sprite, nx, ny );
				if ( !c.visible ) {
					c.visible = true;
					PS.spriteShow( c.sprite, true );
				}
			}

			// Otherwise hide sprite

			else if ( c.visible ) {
				c.visible = false;
				PS.spriteShow( c.sprite, false );
			}
		}
	}

	// Move character to map position x/y

	function moveCharacter ( c, x, y ) {
		c.mx = x;
		c.my = y;
	}

	// Start character along a route (if it has any steps)

	function startRoute ( c, route ) {
		if ( route.length > 0 ) {
			c.route = route;
		}
		else {
			c.route = null;
		}
		c.step = 0;
	}

	function scroll ( x, y ) {
		var len, i, c;

		if ( x < 0 ) {
			x = 0;
		}
		if ( x > xmax ) {
			x = xmax;
		}
		left = x;

		if ( y < 0 ) {
			y = 0;
		}
		if ( y > ymax ) {
			y = ymax;
		}
		top = y;

		drawBackground();
		drawForeground();

		drawCharacter( player );
		drawCharacter( target );

		len = npc1.length;
		for ( i = 0; i < len; i += 1 ) {
			c = npc1[ i ];
			drawCharacter( c );
		}

		len = npc2.length;
		for ( i = 0; i < len; i += 1 ) {
			c = npc2[ i ];
			drawCharacter( c );
		}

	}
	// Called when map needs scrolling

	function scroller () {
		var dx, dy;

		if ( left > scroll_x ) {
			dx = -1;
		}
		else if ( left < scroll_x ) {
			dx = 1;
		}
		else {
			dx = 0;
		}

		if ( top > scroll_y ) {
			dy = -1;
		}
		else if ( top < scroll_y ) {
			dy = 1;
		}
		else
		{
			dy = 0;
		}

		if ( dx || dy )	{
			scroll( left + dx, top + dy );
		}
		else {
			scrolling = false;
		}

//		var route, len, step, pos, x, y;
//
//		route = scroll_path;
//		if ( route ) {
//			len = route.length;
//			step = scroll_step;
//			if ( step < len ) {
//				pos = route[ step ]; // get next step on route
//				x = pos[ 0 ];
//				y = pos[ 1 ];
//				scroll( x, y );
//				step += 1;
//				if ( step >= len ) { // path done?
//					scrolling = false;
//					scroll_path = null; // nuke it
//					step = 0;
//				}
//				scroll_step = step;
//			}
//		}
	}

	// Check if scrolling required for object at map coordinates [x, y]
	// Returns true if scroll needed, else false

	function scrollCheck ( mx, my ) {
		var any, x, y, nx, ny;

		any = false;

		// calc effective grid coordinates

		x = mx - left;
		y = my - top;

		if ( x < episode.margin_left ) // scroll left?
		{
			if ( left > 0 )
			{
				nx = left - episode.scroll_left;
				if ( nx < episode.margin_left )	{
					nx = 0;
				}
				scroll_x = nx;
				any = true;
//				PS.debug( "Starting scroll left\n" );
			}
		}
		else if ( x > episode.margin_right ) // scroll right?
		{
			if ( left < xmax )
			{
				nx = left + episode.scroll_right;
				if ( nx > xmax ) {
					nx = xmax;
				}
				scroll_x = nx;
				any = true;
//				PS.debug( "Starting scroll right\n" );
			}
		}
		else {
			scroll_x = left;
		}

		if ( y < episode.margin_top ) // scroll up?
		{
			if ( top > 0 )
			{
				ny = top - episode.scroll_up;
				if ( ny < episode.margin_top ) {
					ny = 0;
				}
				scroll_y = ny;
				any = true;
//				PS.debug( "Starting scroll up\n" );
			}
		}
		else if ( y > episode.margin_bottom ) // scroll down?
		{
			if ( top < ymax )
			{
				ny = top + episode.scroll_down;
				if ( ny > ymax ) {
					ny = ymax;
				}
				scroll_y = ny;
				any = true;
//				PS.debug( "Starting scroll down\n" );
			}
		}
		else {
			scroll_y = top;
		}

		scrolling = any;
		return any;
	}

	// Timer function for Episode 1
	//TODO: copy and adjust for dream1_tick
	function ep1_tick ()
	{
		//TODO: call animate character on npc types
		animateCharacter(player);
		animateCharacter(target);
		//animateCharacter(player);
		//animateCharacter(player);
		//animateCharacter(player);

		if ( scrolling ) {
			scroller();
		}

		triggerCheck();
		//TODO: Code a function to see if the player is on a camera trigger, call here
		//Early triggers will just display text
		//Camera triggers will likely just scroll the map to certain locations
	}

	function animateCharacter(c){
		var route, len, step, pos, x, y;

		route = c.route;
		if ( route )
		{
			len = route.length;
			step = c.step;

			if ( step < len )
			{
				pos = route[ step ]; // get next step on route
				x = pos[ 0 ];
				y = pos[ 1 ];

				// Stop if move would enter a wall

				if ( !isWall ( c.pathmap, x, y ) ) {

					moveCharacter( c, x, y );// point to next path step
					step += 1;
					if ( step >= len ) { // path done?
						c.route = null; // nuke it
						step = 0;
					}

					// Redraw if scrolling not needed

					if ( !scrollCheck( x, y ) )	{
						drawCharacter( c );
					}
				}
				else { // stop if wall
					c.route = null; // nuke route
					step = 0;
				}
				c.step = step;
			}
		}


	}

	//TODO: comment the trigger handling code
	function triggerCheck(){
		var len, i, j, sendindex;

		len = triggerArray.length;
		if ( player.mx !== previousX || player.my !== previousY ) {
			for (i = 0; i < len; i += 1) {
				if (player.mx === triggerArray[i].mx && player.my === triggerArray[i].my && triggerArray[i].active === true) {
					triggerHandler(triggerArray[i].index);
					sendindex = triggerArray[i].index;
	//				PS.debug("DEBUGGING TRIGGERCHECK: " + sendindex + " | " + triggerArray[i].index);

					if (triggerArray[i].type === 1) {
						for (j = 0; j < len; j += 1) {
							if (triggerArray[j].index === sendindex) {
								triggerArray[j].active = false;
							}
						}
					}
					if (triggerArray[i].type === 2) {
						triggerArray[i].active = false;
					}
				}
			}
			previousX = player.mx;
			previousY = player.my;
		}
	}

	function triggerHandler(index){
		if ( episode.id === "dream1" ){
			runTriggerDream1(index);
		}
		if ( episode.id === "ep1" ){
			runTriggerEp1(index);
		}
	//repeat for all episodes
	}

	//HEY SHANE YOU CAN EDIT THIS PART
	function runTriggerEp1(index){
		switch ( index ) {
			case 1:
				displayText(PS.COLOR_BLACK, "Dream 1 (move right)", 150);
				sys.loadEpisode("dream1");
				break;

			case 2:
				displayText(PS.COLOR_BLACK, "Good morning sir, did you sleep well?", 150);
				break;

			case 3:
				displayText(PS.COLOR_BLACK, "Your table is right here, sir.", 150);
				break;

			case 4:
				//TODO: shorten this line
				displayText(PS.COLOR_BLACK, "Enjoy your stroll around the city this afternoon.", 150);
				break;

			case 5:
				displayText(PS.COLOR_BLACK, "A lovely day, isn't it!", 150);
				break;

			case 6:
				//red man at beach
				displayText(PS.COLOR_BLACK, "Your sweetheart, your pretty sweetheart!", 150);
				break;

			case 7:
				//bottom dining room
				displayText(PS.COLOR_BLACK, "Beona sera. This way, signore.", 150);
				break;

			case 8:
				//top dining room
				displayText(PS.COLOR_BLACK, "I'm sorry sir, I'm afraid dinner is over.", 150);
				break;

			case 9:
				//at ending desk, both paths
				displayText(PS.COLOR_BLACK, "Dormire bene, signore.", 150);
				delayText(180, PS.COLOR_BLACK, "Wishing you pleasant dreams...", 150);
				break;

			//repeat for all indexes
		}
	}

	function runTriggerDream1(index){
		switch ( index ) {
			case 1:
				PS.statusText("This should never be seen");
				break;

			//repeat for all indexes
		}
	}

	//Text displaying functions.
	//displayText takes the text color, the contents of the text and the amount of time it should be visible.
	//delayText takes a delay value in 60ths of a second and the above arguments.

	function displayText(textColor, text, duration) {
		PS.statusFade(0);
		PS.statusColor(textColor);
		PS.statusText(text);
		PS.statusFade(duration);
		PS.statusColor(PS.COLOR_WHITE);
		if (textTimer !== null) {
			PS.timerStop(textTimer);
			textTimer = null;
		}
	}

	function delayText(wait, textColor, text, duration){
		textTimer = PS.timerStart(wait, displayText, textColor, text, duration);
	}

	// Start a loaded episode

	function startEpisode () {
		var len, i, c;

		PS.gridSize( episode.grid_x, episode.grid_y ); // set grid size
		PS.border( PS.ALL, PS.ALL, 0 ); // hide bead borders

		// Establish map parameters

		width = image_bg.width;
		height = image_bg.height;

		xmax = width - episode.grid_x;
		if ( xmax < 0 ) {
			xmax = 0;
		}

		ymax = height - episode.grid_y;
		if ( ymax < 0 ) {
			ymax = 0;
		}

		left = episode.start_left;
		top = episode.start_top;

		episode.margin_right = episode.grid_x - episode.margin_right;
		episode.margin_bottom = episode.grid_y - episode.margin_bottom;

		drawBackground();
		drawForeground();

		// Create & place player sprite

		player.sprite = PS.spriteSolid( 1, 1 );
		PS.spriteSolidColor( player.sprite, player.color );
		PS.spritePlane( player.sprite, PLANE_PLAYER );
		moveCharacter( player, player.mx, player.my );
		drawCharacter( player );

		// Create target sprite

		target.sprite = PS.spriteSolid( 1, 1 );
		PS.spriteSolidColor( target.sprite, target.color );
		PS.spritePlane( target.sprite, PLANE_TARGET );
		moveCharacter( target, target.mx, target.my );
		drawCharacter( target );

		// Create NPC1 sprites

		len = npc1.length;
		for ( i = 0; i < len; i += 1 ) {
			c = npc1[ i ];
			c.sprite = PS.spriteSolid( 1, 1 );
			PS.spriteSolidColor( c.sprite, c.color );
			PS.spritePlane( c.sprite, PLANE_NPC );
			moveCharacter( c, c.mx, c.my );
			drawCharacter( c );
		}

		// Create NPC2 sprites

		len = npc2.length;
		for ( i = 0; i < len; i += 1 ) {
			c = npc2[ i ];
			c.sprite = PS.spriteSolid( 1, 1 );
			PS.spriteSolidColor( c.sprite, c.color );
			PS.spritePlane( c.sprite, PLANE_NPC );
			moveCharacter( c, c.mx, c.my );
			drawCharacter( c );
		}

		// Stop previous episode's timer (if any )

		if ( timer ) {
			PS.timerStop( timer );
			timer = null;
		}

		// Start new episode's timer (if any)

		if ( episode.timer ) {
			timer = PS.timerStart( 4, episode.timer ); // tick at 15 fps
		}
	}

	// Set up a new trigger object
	// (required) str : string = unique id string
	// (required) x, y : number = initial x/y position
	// (required) g, b : number = index and type of trigger
	// Returns initialized trigger object

	function initTrigger ( str, x, y, g, b ) {
		var t;

		t = {
			id : str, // unique id string
			mx : x, // x position on map
			my : y, // y position on map
			index : g, // index
			type : b, //type
			active : true //true if the trigger is active, false if it's not.
						  //currently all triggers are active by default.
		};

		PS.debug( str + " @ " + x + ", " + y + ", " + g + ", " + b + ", " + t.active + "\n" );

		return t;
	}

	// Set up a new character object
	// (required) str : string = unique id string
	// (required) x, y : number = initial x/y position
	// (optional) rgb : number = RGB multiplex color (default = red)
	// (optional) pm : string = pathmap (default = episode pathmap)
	// Returns initialized character object

	function initCharacter ( str, x, y, rgb, pm ) {
		var c;

		if ( rgb === undefined ) {
			rgb = PS.COLOR_MAGENTA;
		}

		if ( pm === undefined ) {
			pm = pathmap; // use episode map
		}

		c = {
			id : str, // unique id string
			mx : x, // x position on map
			my : y, // y position on map
			gx : -5000, // x position on grid
			gy : -5000, // y position on grid
			color : rgb, // color,
			visible : false, // true if visible
			sprite : "", // sprite id
			pathmap : pm, // pathmap
			route : null, // current route, null if none
			step : 0 // current path step
		};

		if( c.id !== "player" && c.id !== "target") {
			makeWall ( pm, x, y );
		}

		PS.debug( str + " @ " + x + ", " + y + "\n" );

		return c;
	}

	// Load trigger image

	function loadTriggers ( imageData ) {
		var ptr, len, x, y, r, g, b, id;

		// Report imageData in debugger

		PS.debug( "Loaded trigger map " + imageData.source +
			":\nid = " + imageData.id +
			"\nwidth = " + imageData.width +
			"\nheight = " + imageData.height +
			"\nformat = " + imageData.pixelSize + "\n" );

		image_triggers = imageData; // save character image in case needed later

		// Scan image data to extract character locations

		len = 0;
		ptr = 0;
		for ( y = 0; y < imageData.height; y += 1 ) {
			for ( x = 0; x < imageData.width; x += 1 ) {

				// Get r/g/b values of image pixel

				r = imageData.data[ ptr ];
				ptr += 1;
				g = imageData.data[ ptr ];
				ptr += 1;
				b = imageData.data[ ptr ];
				ptr += 1;

				// If red = 128, it's a trigger

				if ( r === 128 ){
					len += 1;
					id = "trigger_index_" + g + "_type_" + b + "_pos_" + len;
					triggerArray.push( initTrigger( id, x, y, g, b ) );
				}
			}
		}

		startEpisode(); // everything's loaded!
	}

	//Load character image

	function loadCharacters ( imageData ) {
		var ptr, len, x, y, r, g, b, id;

		// Report imageData in debugger

		PS.debug( "Loaded character map " + imageData.source +
			":\nid = " + imageData.id +
			"\nwidth = " + imageData.width +
			"\nheight = " + imageData.height +
			"\nformat = " + imageData.pixelSize + "\n" );

		image_chars = imageData; // save character image in case needed later

		// Scan image data to extract character locations

		ptr = 0;
		for ( y = 0; y < imageData.height; y += 1 ) {
			for ( x = 0; x < imageData.width; x += 1 ) {

				// Get r/g/b values of image pixel

				r = imageData.data[ ptr ];
				ptr += 1;
				g = imageData.data[ ptr ];
				ptr += 1;
				b = imageData.data[ ptr ];
				ptr += 1;

				// Determine the type of NPC from the color

				if ( r === 255 && g === 255 && b === 0 ) {
					id = "target";
					target = initCharacter( id, x, y, COLOR_TARGET );
				}
				if ( r === 0 && g === 255 && b === 0 ) {
					id = "player";
					player = initCharacter( id, x, y, COLOR_PLAYER );
				}
				if ( r === 255 && b === 0 ) { // NPC1
					id = "npc1_" + g;
					npc1.push( initCharacter( id, x, y, COLOR_NPC1 ) );
				}
				if ( r === 0 && b === 255 ) { // NPC2
					id = "npc2_" + g;
					npc2.push( initCharacter( id, x, y, COLOR_NPC2 ) );
				}
			}
		}

		// Load triggers

		PS.imageLoad( episode.file_triggers, loadTriggers, 3 );
	}

	// Load pathmap image

	function loadPathmap ( imageData ) {
		var map, ptr, len, i, r, g, b;

		// Report imageData in debugger

		PS.debug( "Loaded pathmap " + imageData.source +
			":\nid = " + imageData.id +
			"\nwidth = " + imageData.width +
			"\nheight = " + imageData.height +
			"\nformat = " + imageData.pixelSize + "\n" );

		image_pathmap = imageData; // save pathmap image in case needed later

		// Create a new imageMap by analyzing data in pathmap image

		map = {
			width : imageData.width,
			height : imageData.height,
			pixelSize : 1,
			data : []
		};

		ptr = 0;
		len = imageData.width * imageData.height;
		for ( i = 0; i < len; i += 1 ) {
			// Get r/g/b values of image pixel

			r = imageData.data[ ptr ];
			ptr += 1;
			g = imageData.data[ ptr ];
			ptr += 1;
			b = imageData.data[ ptr ];
			ptr += 1;

			// If all values are below black threshold, assume wall

			if ( ( r < THRESHOLD ) && ( g < THRESHOLD ) && ( b < THRESHOLD ) ) {
				map.data[ i ] = WALL_VALUE;
			}
			else {
				map.data[ i ] = FLOOR_VALUE; // otherwise make it walkable
			}
		}

		pathmap = PS.pathMap( map ); // Create pathmap

		// Load characters

		PS.imageLoad( episode.file_chars, loadCharacters, 3 );
	}

	// Load background and foreground images

	function loadForeground ( imageData ) {
	//	var x, y, ptr, color;

		// Report imageData in debugger

		PS.debug( "Loaded background " + imageData.source +
			":\nid = " + imageData.id +
			"\nwidth = " + imageData.width +
			"\nheight = " + imageData.height +
			"\nformat = " + imageData.pixelSize + "\n" );

		image_fg = imageData; // save image data

		// Load pathmap & characters

		PS.imageLoad( episode.file_pathmap, loadPathmap, 3 );
	}
	function loadBackground ( imageData ) {
	//	var x, y, ptr, color;

		// Report imageData in debugger

		PS.debug( "Loaded background " + imageData.source +
			":\nid = " + imageData.id +
			"\nwidth = " + imageData.width +
			"\nheight = " + imageData.height +
			"\nformat = " + imageData.pixelSize + "\n" );

		image_bg = imageData; // save image data

		// Load foreground, pathmap & characters

		PS.imageLoad( episode.file_fg, loadForeground, 4 );
	}

	// Returns true if pathmap location x/y is a wall

	function isWall ( pm, x, y ) {
		var result, val;

		result = PS.pathData( pm, x, y, 1, 1 );
		val = result[ 0 ];
		return ( val <= WALL_VALUE );
	}

	// Change pathmap location x/y to a wall

	function makeWall ( pm, x, y ) {
		var result;

		PS.pathData( pm, x, y, 1, 1, 0 );
	}

	// Change pathmap location x/y to a floor

	function makeFloor ( pm, x, y ) {
		var result;

		PS.pathData( pm, x, y, 1, 1, 1 );
	}

	// Properties of sys become public properties of G

	var sys = {

		// Load an episode
		// str : string = id of an episode in the episodes object

		loadEpisode : function ( str ) {

			// Validate id string

			if ( !str || ( typeOf( str ) !== "string" ) || ( str.length < 1 ) ) {
				PS.debug( "[G.loadEpisode] Invalid parameter" );
				return PS.ERROR;
			}

			// Validate episode

			episode = episodes[ str ];
			if ( !episode || ( typeOf( episode ) !== "object" ) ) {
				PS.debug( "[G.loadEpisode] '" + str + "' is not a valid episode id" );
				return PS.ERROR;
			}

			// Load background image (which also loads walkmap and characters)

			PS.imageLoad( episode.file_bg, loadBackground, 1 );

			return PS.DONE;
		},

		// G.click( x, y, data, options )
		// Called when grid is clicked

		click : function ( x, y, data, options )
		{
			var nx, ny, map, route, len, i, step, x, y;

			// If clicking current player loc, exit (for now)

			if ( ( x === player.gx ) && ( y === player.gy ) ) {
				return;
			}

			map = player.pathmap;

			// Convert clicked grid location to map coordinates

			nx = x + left;
			ny = y + top;

			// Is clicked location a wall?
			// If not, call pathfinder

			if ( !isWall( map, nx, ny ) ) {
				route = PS.pathFind( map, player.mx, player.my, nx, ny );
			}

			// Otherwise follow a straight line to target
			// Player will stop at first wall
			// This needs to be smarter ...

			else {
				route = PS.line( player.mx, player.my, nx, ny );

				// Cut off line at first wall

				len = route.length;
				for ( i = 0; i < len; i += 1 ) {
					step = route[ i ];
					x = step[ 0 ];
					y = step[ 1 ];
					if ( isWall( map, x, y ) ) {
						route.length = i;
						break;
					}
				}
			}

			startRoute( player, route );
		}
	};

	return sys;
} () );

// All of the functions below MUST exist, or the engine will complain!

PS.init = function ( system, options ) {
	"use strict";

	G.loadEpisode( "ep1" );
};

PS.touch = function ( x, y, data, options ) {
	"use strict";

	G.click( x, y, data, options );
};

PS.release = function ( x, y, data, options ) {
	"use strict";
};

PS.enter = function ( x, y, data, options ) {
	"use strict";
};

PS.exit = function ( x, y, data, options ) {
	"use strict";
};

PS.exitGrid = function ( options ) {
	"use strict";
};

PS.keyDown = function ( key, shift, ctrl, options ) {
	"use strict";
};

PS.keyUp = function ( key, shift, ctrl, options ) {
	"use strict";
};

PS.swipe = function ( data, options ) {
	"use strict";
};

PS.input = function ( sensors, options ) {
	"use strict";
};


