Main timeline design: discrete "rooms" as a progression through time
	A semi-blur/fade effect directly in the map to denote this to the player
	Each "room" will have several objects to show what each room is (for example, a front desk and some couches for a hotel lobby) and possibly an NPC for an additional contextual clue
	Only fades to the left side/from the right, with a shift into each new area forwards
	Ensure the player doesn't see too far from the start: no visible large blurs until after the player has moved forward
	
Alpha contents:
	One "episode"
	All major features implemented in some form
	Tadzio AI will likely be weak
	All code modular for easy extensibility in B Term