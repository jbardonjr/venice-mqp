// game.js for Perlenspiel 3.2

/*
Perlenspiel is a scheme by Professor Moriarty (bmoriarty@wpi.edu).
Perlenspiel is Copyright © 2009-15 Worcester Polytechnic Institute.
This file is part of Perlenspiel.

Perlenspiel is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Perlenspiel is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You may have received a copy of the GNU Lesser General Public License
along with Perlenspiel. If not, see <http://www.gnu.org/licenses/>.

Perlenspiel uses dygraphs (Copyright © 2009 by Dan Vanderkam) under the MIT License for data visualization.
See dygraphs License.txt, <http://dygraphs.com> and <http://opensource.org/licenses/MIT> for more information.
*/

// The following comment lines are for JSLint. Don't remove them!

/*jslint nomen: true, white: true */
/*global PS */

// This is a template for creating new Perlenspiel games

// All of the functions below MUST exist, or the engine will complain!

// PS.init( system, options )
// Initializes the game
// This function should normally begin with a call to PS.gridSize( x, y )
// where x and y are the desired initial dimensions of the grid
// [system] = an object containing engine and platform information; see documentation for details
// [options] = an object with optional parameters; see documentation for details

var G; // public namespace variable for game

( function () {
	"use strict";

	//Create map variables

	//Create PC and Tadzio variables

	//Define functions for creating maps
	//the ending variable may be unnecessary with the current ideas.
	/*
	G.LoadMap(int episode, bool ending){
	if(ending==false)
		switch(episode)
			case 1:
				//e1vb stands for episode 1 visual bottom, m and t are for middle and top
				//pf and mp are "PathFinding" and "Main Path"
				PS.ImageLoad(e1vbMain.png, G.GenerateVisualMap, 1)
				PS.ImageLoad(e1vmMain.png, G.GenerateVisualMap, 1)
				PS.ImageLoad(e1vtMain.png, G.GenerateVisualMap, 1)
				PS.ImageLoad(e1pfMain.png, G.GenerateWalkMap, 1)
				PS.ImageLoad(e1mpMain.png, G.GeneratePathMap, 1)
			case 2:
				//and so on...

		ending=true    //to pre-set the values for the next load.
	else               //AppendMap is a variation on GenerateMap
		if(player pursued tadzio)     //Instead of overwriting, it extends the map rightwards.
			PS.ImageLoad(e1vbEndGood.png, G.AppendVisualMap, 1)
			//etc...
		else           //Not actually good or bad, just internal notes
			PS.ImageLoad(e1vbEndBad.png, G.AppendVisualMap, 1)
	episode++
	ending=false
	G.NextEpisode = episode
	G.NextEnding = ending
	}
	*/

	/*
	 G.GenerateVisualMap(ImageData inputdata){
	 	str = inputdata.source
	 	ch = fourth character of str
	 	switch (ch)
	 		case b:
	 			array Visual_Bottom = inputdata.data
	 			Visual_Bottom = Visual_Bottom + Visual_Bottom + Visual_Bottom //make 3 copies
	 		case m:
	 			array Visual_Middle = inputdata.data
	 			//see Visual_Bottom
	 		case t:
	 			array Visual_Top = inputdata.data
	 			//see Visual_Bottom
	 }

	 G.GenerateWalkMap(ImageData inputdata){
	 	array WalkMap
	 	counter = inputdata.height * inputdata.width
	 	while(counter > 0){
	 		counter--
	 	}
	 	if(inputdata.source[counter]==FFFFFF) //white
	 		WalkMap[counter]=1
	 	if(inputdata.source[counter]==000000) //black
	 		WalkMap[counter]=0
	 	if(inputdata.source[counter]==7FFFFF) //shades of gray, etc...
	 		WalkMap[counter]=2 //continued...
	 	WalkMap = WalkMap + WalkMap + WalkMap  //make 3 copies
	 }

	 G.GeneratePathMap(ImageData inputdata){
	 	array PathMap
	 	counter = inputdata.height * inputdata.width
	 	while(counter > 0){
	 		counter--
	 	}
	 	if(inputdata.source[counter]==FFFFFF) //white
	 		PathMap[counter]=0
	 	if(inputdata.source[counter]==000000) //black
	 		PathMap[counter]=6           //Whatever the scrolling threshold needs to be
	 	//Make 3 copies, see WalkMap
	 }
	 */

	/*
	G.Pathfind(entity, newx, newy){
		Destination = MapTopLeft + [newx, newy]
		PS.pathFind (PathMap, entity.x, entity.y, Destination.x, Destination.y)
	}
	*/

	/*
	G.Scroll(TargetTopLeft){
		//Ensure new topleft is within [0,0] and [map.width - screen.width, map.height - screen.height]
		//If it's outside of the boundaries, snap to boundary.
		MapTopLeft = TargetTopLeft
	}
	*/

	/*
	G.Tick(){
		if(Player.Y < MapHeight * 1/6){
			Player.Y += MapHeight/3
			Player.PathfindTarget += MapHeight/3
			Tadzio.PathfindTarget += MapHeight/3
		}
		if(Player.Y > MapHeight * 5/6){
			Player.Y -= MapHeight/3
			Player.PathfindTarget -= MapHeight/3
			Tadzio.PathfindTarget -= MapHeight/3
		}
		//repeat for all moving NPCs

		G.Tadzio() //Determines where, if anywhere, Tadzio needs to move to.
		if(relativePlayerPosition < or > scrollThreshold)
			G.Scroll(NewMapTopLeft)
		//This function will see if more of the map needs to be loaded.
		//could be replaced with an if(nextepisodeisready) with a flag set at the end of the previous episode
		bool ToLoad = G.LoadMore()
		if(ToLoad)
			LoadMap(G.NextEpisode, G.NextEnding)
		G.Draw()
	}
	*/

	/*
	//The walls will be on layer 4, and the floor on layer 1.
	G.Draw(){
		//iterate though PS.width and PS.height as x and y
		PS.Write(Visual_Bottom[MapTopLeft(x)-x, MapTopLeft(y)-y]) //to layer 1
		//Layer 2, boats of any sort, things that go under other things
		PS.Write(Visual_Middle...)  //to layer 3
		//Layer 4, the blurred path drawn only when left of "X coordinate of the player - 2"
	 	//Layer 5, Tadzio's glow.
		//Maybe at the right edge of the screen, too
		//Layer 6, People.
		//Layer 7, Visual_Top
	}
	*/
}

PS.init = function( system, options ) {
	"use strict";

	// Use PS.gridSize( x, y ) to set the grid to
	// the initial dimensions you want (32 x 32 maximum)
	// Do this FIRST to avoid problems!
	// Otherwise you will get the default 8x8 grid

	PS.gridSize( 25, 25 );
	//G.LoadMap(1, false)  //Load the first map
	//PS.timerStart(4, G.Tick())  //Begin the game loop
};

// PS.touch ( x, y, data, options )
// Called when the mouse button is clicked on a bead, or when a bead is touched
// It doesn't have to do anything
// [x] = zero-based x-position of the bead on the grid
// [y] = zero-based y-position of the bead on the grid
// [data] = the data value associated with this bead, 0 if none has been set
// [options] = an object with optional parameters; see documentation for details

PS.touch = function( x, y, data, options ) {
	"use strict";

	// Uncomment the following line to inspect parameters
	// PS.debug( "PS.touch() @ " + x + ", " + y + "\n" );

	// Add code here for mouse clicks/touches over a bead
	//G.Pathfind(player, x, y)  //Tell the player to move to a location on the screen
};

// PS.release ( x, y, data, options )
// Called when the mouse button is released over a bead, or when a touch is lifted off a bead
// It doesn't have to do anything
// [x] = zero-based x-position of the bead on the grid
// [y] = zero-based y-position of the bead on the grid
// [data] = the data value associated with this bead, 0 if none has been set
// [options] = an object with optional parameters; see documentation for details

PS.release = function( x, y, data, options ) {
	"use strict";

	// Uncomment the following line to inspect parameters
	// PS.debug( "PS.release() @ " + x + ", " + y + "\n" );

	// Add code here for when the mouse button/touch is released over a bead
};

// PS.enter ( x, y, button, data, options )
// Called when the mouse/touch enters a bead
// It doesn't have to do anything
// [x] = zero-based x-position of the bead on the grid
// [y] = zero-based y-position of the bead on the grid
// [data] = the data value associated with this bead, 0 if none has been set
// [options] = an object with optional parameters; see documentation for details

PS.enter = function( x, y, data, options ) {
	"use strict";

	// Uncomment the following line to inspect parameters
	// PS.debug( "PS.enter() @ " + x + ", " + y + "\n" );

	// Add code here for when the mouse cursor/touch enters a bead
};

// PS.exit ( x, y, data, options )
// Called when the mouse cursor/touch exits a bead
// It doesn't have to do anything
// [x] = zero-based x-position of the bead on the grid
// [y] = zero-based y-position of the bead on the grid
// [data] = the data value associated with this bead, 0 if none has been set
// [options] = an object with optional parameters; see documentation for details

PS.exit = function( x, y, data, options ) {
	"use strict";

	// Uncomment the following line to inspect parameters
	// PS.debug( "PS.exit() @ " + x + ", " + y + "\n" );

	// Add code here for when the mouse cursor/touch exits a bead
};

// PS.exitGrid ( options )
// Called when the mouse cursor/touch exits the grid perimeter
// It doesn't have to do anything
// [options] = an object with optional parameters; see documentation for details

PS.exitGrid = function( options ) {
	"use strict";

	// Uncomment the following line to verify operation
	// PS.debug( "PS.exitGrid() called\n" );

	// Add code here for when the mouse cursor/touch moves off the grid
};

// PS.keyDown ( key, shift, ctrl, options )
// Called when a key on the keyboard is pressed
// It doesn't have to do anything
// [key] = ASCII code of the pressed key, or one of the following constants:
// Arrow keys = PS.ARROW_UP, PS.ARROW_DOWN, PS.ARROW_LEFT, PS.ARROW_RIGHT
// Function keys = PS.F1 through PS.F1
// [shift] = true if shift key is held down, else false
// [ctrl] = true if control key is held down, else false
// [options] = an object with optional parameters; see documentation for details

PS.keyDown = function( key, shift, ctrl, options ) {
	"use strict";

	// Uncomment the following line to inspect parameters
	//	PS.debug( "DOWN: key = " + key + ", shift = " + shift + "\n" );

	// Add code here for when a key is pressed
};

// PS.keyUp ( key, shift, ctrl, options )
// Called when a key on the keyboard is released
// It doesn't have to do anything
// [key] = ASCII code of the pressed key, or one of the following constants:
// Arrow keys = PS.ARROW_UP, PS.ARROW_DOWN, PS.ARROW_LEFT, PS.ARROW_RIGHT
// Function keys = PS.F1 through PS.F12
// [shift] = true if shift key is held down, false otherwise
// [ctrl] = true if control key is held down, false otherwise
// [options] = an object with optional parameters; see documentation for details

PS.keyUp = function( key, shift, ctrl, options ) {
	"use strict";

	// Uncomment the following line to inspect parameters
	// PS.debug( "PS.keyUp(): key = " + key + ", shift = " + shift + ", ctrl = " + ctrl + "\n" );

	// Add code here for when a key is released
};

// PS.swipe ( data, options )
// Called when a mouse/finger swipe across the grid is detected
// It doesn't have to do anything
// [data] = an object with swipe information; see documentation for details
// [options] = an object with optional parameters; see documentation for details

PS.swipe = function( data, options ) {
	"use strict";

	// Uncomment the following block to inspect parameters

	/*
	 var len, i, ev;
	 PS.debugClear();
	 PS.debug( "PS.swipe(): start = " + data.start + ", end = " + data.end + ", dur = " + data.duration + "\n" );
	 len = data.events.length;
	 for ( i = 0; i < len; i += 1 ) {
	 ev = data.events[ i ];
	 PS.debug( i + ": [x = " + ev.x + ", y = " + ev.y + ", start = " + ev.start + ", end = " + ev.end +
	 ", dur = " + ev.duration + "]\n");
	 }
	 */

	// Add code here for when an input event is detected
};

// PS.input ( sensors, options )
// Called when an input device event (other than mouse/touch/keyboard) is detected
// It doesn't have to do anything
// [sensors] = an object with sensor information; see documentation for details
// [options] = an object with optional parameters; see documentation for details

PS.input = function( sensors, options ) {
	"use strict";

	// Uncomment the following block to inspect parameters
	/*
	PS.debug( "PS.input() called\n" );
	var device = sensors.wheel; // check for scroll wheel
	if ( device )
	{
		PS.debug( "sensors.wheel = " + device + "\n" );
	}
	*/
	
	// Add code here for when an input event is detected
};

